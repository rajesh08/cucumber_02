package com.selenium.Array;

import java.util.Scanner;

public class Linear_Search {
	/* Program: Linear Search
	 * Input: Number of elements, element's values, value to be searched
	 * Output:Position of the number input by user among other numbers*/
	public static void main(String[] args) {
		int array[],counter,item_no;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of elements in the Array:\n");
		int num=sc.nextInt();
		array=new int[num];
		System.out.println("Enter"+num+"Integers:\n");
		for (counter = 0; counter < array.length; counter++)
			array[counter]=sc.nextInt();
		System.out.println("Enter the search value:\n");
		item_no=sc.nextInt();
		
		for ( counter = 0; counter < num; counter++) {
			if (array[counter]==item_no) {
				
				System.out.println(item_no+"is present at the location"+(counter+1));
				break;
			}
		}
	if (counter==num) {
		System.out.println(item_no+"does not exist in the array");
	}	
			
		
	}

}
