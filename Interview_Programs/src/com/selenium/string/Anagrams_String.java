package com.selenium.string;

public class Anagrams_String {
//program to check whether a string is a Palindrome
	public static void main(String[] args) {
		String str = "abba"; 
		  
        if (palindrome(str)) 
            System.out.print("Yes"); 
        else
            System.out.print("No"); 

	}
	
	static boolean palindrome(String s) {
		
		int i=0,j=s.length()-1;
		
		while (i<j) {
			if(s.charAt(i)!=s.charAt(j)) 
				return false;
			i++;
			j--;
			
		}
		
		
		return true;
		
	}

}
